**Darker-Mofit**

This theme is a dark modification of the xfwm Mofit theme, for use with
dark themes such as Qogir-win-dark, Arc-Dark and Breeze-Dark.
This theme also removes the window button restrictions of the normal xfwm Mofit theme.

To install this window manager theme, just put the git repo in `~/.themes`
